import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { NewsCommander } from './modules/news/news.commander';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  app.useGlobalPipes(new ValidationPipe({
    transform: true,
    whitelist: true,
    forbidNonWhitelisted:true,
    transformOptions: {
      enableImplicitConversion: true
    }
  }));
  
  const newsCommander = app.get<NewsCommander>(NewsCommander);
  const getNewsPromise = newsCommander.getNews();
  await getNewsPromise;
  await app.listen(3000);
}
bootstrap();
