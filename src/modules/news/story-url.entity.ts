import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { HighlightResultsEntity } from "./highlightresults.entity";

@Entity({name: 'Story_url'})
export class Story_urlEntity{

    @PrimaryGeneratedColumn('increment')
    id:number;
    
    @Column()
    value:string;

    @Column()
    matchLevel:string;
    
    @Column()
    matchedWords:string;

    @OneToOne((type) => HighlightResultsEntity, (highlightResult) => highlightResult.story_url)
    _highlightResult: HighlightResultsEntity;

}