import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

import { CreateNewsDto, PaginationQueryDto,  } from '../dto';
import { NewsEntity } from '../news.entity';
import { HighlightResultsEntity } from '../highlightresults.entity';
import { AuthorEntity } from '../author.entity';
import { Comment_textEntity } from '../comment-text.entity';
import { Story_titleEntity } from '../story-title.entity';
import { Story_urlEntity } from '../story-url.entity';
import { HttpService } from '@nestjs/axios';
import { ConnectableObservable } from 'rxjs';
import { AxiosResponse } from 'axios';

@Injectable()
export class NewsService {

    constructor(
        private httpService: HttpService,
        @InjectRepository(NewsEntity) private readonly newsRepository: Repository<NewsEntity>,
        @InjectRepository(HighlightResultsEntity) private readonly highlightResultsRepository: Repository<HighlightResultsEntity>,
        @InjectRepository(AuthorEntity) private readonly authorRepository: Repository<AuthorEntity>,
        @InjectRepository(Comment_textEntity) private readonly comment_textRepository: Repository<Comment_textEntity>,
        @InjectRepository(Story_titleEntity) private readonly story_titleRepository: Repository<Story_titleEntity>,
        @InjectRepository(Story_urlEntity) private readonly story_urlRepository: Repository<Story_urlEntity>
    ){

    }

    async getNews({limit,offset}: PaginationQueryDto): Promise<NewsEntity[]>{
        return await this.newsRepository.find({
            where:{ 
                status: 'ACTIVO'
            }, 
            relations: [
                '_highlightResult', 
                '_highlightResult.author',
                '_highlightResult.comment_text', 
                '_highlightResult.story_title',
                '_highlightResult.story_url'
            ],
            skip: offset,
            take: limit
            });
    }

    async getNewsById(id: number): Promise<NewsEntity>{
        const news: NewsEntity = await this.newsRepository.findOne({where: {id: id}, relations: ['_highlightResult', '_highlightResult.author','_highlightResult.comment_text', '_highlightResult.story_title','_highlightResult.story_url']});
        
        if(!news){
            throw new NotFoundException('Resourse not found');
        }

        return news;
    }

    async getNewsByAuthor(author: string): Promise<NewsEntity[]>{
        const news: NewsEntity[] = await this.newsRepository.find({where: {
            status: 'ACTIVO'
        }, relations: ['_highlightResult', '_highlightResult.author','_highlightResult.comment_text', '_highlightResult.story_title','_highlightResult.story_url']})

        const newsByAuthor: NewsEntity[] = news.filter(news => news.author !=null && news.author.includes(author));
        if(!newsByAuthor){
            throw new NotFoundException('Resourse not found');
        }

        return newsByAuthor;
    }

    async getNewsByTags(tags: string): Promise<NewsEntity[]>{
        const news: NewsEntity[] = await this.newsRepository.find({where: {
            status: 'ACTIVO'
        }, relations: ['_highlightResult', '_highlightResult.author','_highlightResult.comment_text', '_highlightResult.story_title','_highlightResult.story_url']})

        const newsByTag: NewsEntity[] = news.filter(news => news._tags !=null && news._tags.includes(tags));
        
        if(!newsByTag){
            throw new NotFoundException('Resourse not found');
        }

        return newsByTag;
    }

    async getNewsByTitle(title: string): Promise<NewsEntity[]>{
        const news: NewsEntity[] = await this.newsRepository.find({where: {
            status: 'ACTIVO'
        }, relations: ['_highlightResult', '_highlightResult.author','_highlightResult.comment_text', '_highlightResult.story_title','_highlightResult.story_url']})
        
        const newsByTitle: NewsEntity[] = news.filter(news => news.title != null && news.title.includes(title));

        if(!newsByTitle){
            throw new NotFoundException('Resourse not found');
        }

        return newsByTitle;
    }

    async createNews(newsDto: CreateNewsDto){
        const exist = await this.newsRepository.findOne({where:{ objectID: newsDto.objectID }})
        if(exist){
            return;
        }
        const news: NewsEntity = this.newsRepository.create(newsDto)
        return this.newsRepository.save(news);
    }

    async updateNews(id:number){
        const news : NewsEntity = await this.newsRepository.preload({
            id,
            status: 'INACTIVO'
        });

        if(!news){
            throw new NotFoundException('Resourse not found');
        }

        return this.newsRepository.save(news);
    }

    async removeNews(id: number): Promise<void>{
        const news : NewsEntity = await this.newsRepository.findOneById(id);
        
        if(!news){
            throw new NotFoundException('Resourse not found');
        }

        this.newsRepository.remove(news);
    }

    getNewsFromUrl() : Promise<AxiosResponse<any>>{
        return this.httpService.axiosRef.get(`https://hn.algolia.com/api/v1/search_by_date?query=nodejs`);
        
    }

}
