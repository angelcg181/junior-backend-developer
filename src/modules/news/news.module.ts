import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { NewsController } from './controller/news.controller';
import { NewsService } from './services/news.service';
import { NewsEntity } from './news.entity';
import { HighlightResultsEntity } from './highlightresults.entity';
import { AuthorEntity } from './author.entity';
import { Comment_textEntity } from './comment-text.entity';
import { Story_titleEntity } from './story-title.entity';
import { Story_urlEntity } from './story-url.entity';
import { HttpModule, HttpService } from '@nestjs/axios';
import { NewsCommander } from './news.commander';

@Module({
    imports: [
        TypeOrmModule.forFeature([
            NewsEntity,
            HighlightResultsEntity,
            AuthorEntity,
            Comment_textEntity,
            Story_titleEntity,
            Story_urlEntity
        ]),
        HttpModule
    ],
    controllers: [NewsController],
    providers: [NewsService, NewsCommander]
})
export class NewsModule {}
