import { Injectable, NotFoundException } from "@nestjs/common";
import { Cron, CronExpression } from "@nestjs/schedule";
import { ConnectableObservable } from "rxjs";
import { NewsEntity } from "./news.entity";

import { NewsService } from "./services/news.service";

@Injectable()
export class NewsCommander{

    constructor (
        private newsService: NewsService){}


    @Cron(CronExpression.EVERY_HOUR)
    async getNews(){
        let i = 0;
        const resp = await this.newsService.getNewsFromUrl();
        
        if(!resp){

            throw new NotFoundException('Resourse not found');

        }

        const data = resp.data.hits;
        data.forEach(async news => {
            try{
                news.status = 'ACTIVO';
                await this.newsService.createNews(news);
            }catch(e){
                console.log(e)
            }
        })


        return resp.data
    }
}