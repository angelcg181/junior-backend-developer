import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { HighlightResultsEntity } from "./highlightresults.entity";

@Entity({name: 'Comment_text'})
export class Comment_textEntity{

    @PrimaryGeneratedColumn('increment')
    id:number;

    @Column()
    value:string;

    @Column()
    matchLevel:string;
    
    @Column()
    fullyHighlighted:string;
    
    @Column()
    matchedWords:string;

    @OneToOne((type) => HighlightResultsEntity, (highlightResult) => highlightResult.comment_text)
    _highlightResult: HighlightResultsEntity;

}
