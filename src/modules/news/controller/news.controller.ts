import { Body, Controller, Delete, Get, NotFoundException, Param, Patch, Post, Query } from '@nestjs/common';

import { CreateNewsDto, PaginationQueryDto, UpdateNewsDto } from '../dto';
import { NewsEntity } from '../news.entity';
import { NewsService } from '../services/news.service';

@Controller('news')
export class NewsController {

    constructor(private readonly newsService: NewsService) {}

    @Get()
    getNews(@Query() pagination: PaginationQueryDto): Promise<NewsEntity[]> {
        return this.newsService.getNews(pagination);
    }

    @Get(':id') // news/1
    getNewsById(@Param('id') id: number): Promise<NewsEntity> {
        return this.newsService.getNewsById(id);
    }

    @Get('author/:author') 
    getNewsByAuthor(@Param('author') author: string): Promise<NewsEntity[]> {
        return this.newsService.getNewsByAuthor(author);
    }

    @Get('tags/:tags') 
    getNewsByTags(@Param('tags') tags: string): Promise<NewsEntity[]> {
        return this.newsService.getNewsByTags(tags);
    }

    @Get('title/:title')
    getNewsByTitle(@Param('title') title: string): Promise<NewsEntity[]> {
        return this.newsService.getNewsByTitle(title);
    }

    @Post()
    createNews(@Body() news: CreateNewsDto): Promise<NewsEntity>{
        return this.newsService.createNews(news);
    }

    @Patch(':id')
    updateNews(@Param('id') id:number): Promise<NewsEntity> {
        return this.newsService.updateNews(id);
    }

    @Delete(':id')
    removeNews(@Param('id') id: number): Promise<void>{
        return this.newsService.removeNews(id);
    }

}
