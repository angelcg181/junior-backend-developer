import { Column, Entity, JoinColumn, ManyToOne, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

import { HighlightResultsEntity } from "./highlightresults.entity";

@Entity({name: 'News'})
export class NewsEntity{
    @PrimaryGeneratedColumn('increment')
    id: number;

    @Column({nullable: true})
    created_at: string | null;
    
    @Column({nullable: true})
    title: string | null;
    
    @Column({nullable: true})
    url: string | null;
    
    @Column({nullable: true})
    author: string | null;
    
    @Column({nullable: true})
    points: number;
    
    @Column({nullable: true})
    story_text: string | null;
    
    @Column({nullable: true})
    comment_text: string | null;
    
    @Column({nullable: true})
    num_comments: number;
    
    @Column({nullable: true})
    story_id: number;
    
    @Column({nullable: true})
    story_title: string | null;
    
    @Column({nullable: true})
    story_url: string | null;
    
    @Column({nullable: true})
    parent_id: number;
    
    @Column({nullable: true})
    created_at_i: number;

    @Column({nullable: true})
    _tags: string | null;
    
    @Column({nullable: true})
    objectID: string | null;
    
    @Column({nullable: true})
    status: string
    
    @OneToOne((type) => HighlightResultsEntity, (highlightResult) => highlightResult.news, {cascade: true})
    @JoinColumn({name: 'highlightResult_id'})
    _highlightResult: HighlightResultsEntity;
}
