import { IsObject, IsOptional, IsString } from "class-validator";
import { HighlightResultsEntity } from "../highlightresults.entity";

export class CreateNewsDto {

    @IsOptional()
    readonly created_at: string | null;

    @IsOptional()
    readonly title: string | null;

    @IsOptional()
    readonly url: string | null;

    @IsOptional()
    readonly author: string | null;

    @IsOptional()
    readonly points: number;

    @IsOptional()
    readonly story_text: string | null;

    @IsOptional()
    readonly comment_text: string | null;

    @IsOptional()
    readonly num_comments: number;

    @IsOptional()
    readonly story_id: number;

    @IsOptional()
    readonly story_title: string | null;

    @IsOptional()
    readonly story_url: string | null;

    @IsOptional()
    readonly parent_id: number;

    @IsOptional()
    readonly created_at_i: number;

    @IsOptional()
    readonly _tags: string | null;

    @IsOptional()
    readonly objectID: string | null;

    @IsObject()
    readonly _highlightResult: Partial<HighlightResultsEntity>;

    @IsOptional()
    readonly status: string = 'ACTIVO';


}