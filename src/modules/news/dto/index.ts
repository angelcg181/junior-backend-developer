export { CreateNewsDto } from "./create-news.dto"
export { UpdateNewsDto } from "./update-news.dto"
export { PaginationQueryDto} from "./pagination-query.dto"