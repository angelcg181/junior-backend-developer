import { IsString } from "class-validator";

export class UpdateNewsDto {
    
    @IsString()
    readonly message: string;

}
