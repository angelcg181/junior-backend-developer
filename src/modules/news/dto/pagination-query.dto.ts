import { IsNumber, IsPositive } from "class-validator";

export class PaginationQueryDto{
    
    @IsNumber()
    @IsPositive()
    limit: number;
    
    @IsNumber()
    offset: number;
}