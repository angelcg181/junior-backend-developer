import { Column, Entity, JoinColumn, OneToMany, OneToOne, PrimaryGeneratedColumn } from "typeorm";

import { AuthorEntity } from "./author.entity";
import { Comment_textEntity } from "./comment-text.entity";
import { NewsEntity } from "./news.entity";
import { Story_titleEntity } from "./story-title.entity";
import { Story_urlEntity } from "./story-url.entity";

@Entity({name: 'HighlightResults'})
export class HighlightResultsEntity{
    
    @PrimaryGeneratedColumn('increment')
    id:number;

    news: NewsEntity;

    @OneToOne((type) => AuthorEntity, (author) => author._highlightResult,{cascade: true})
    @JoinColumn({name: 'author_id'})
    author: AuthorEntity;

    @OneToOne((type) => Comment_textEntity, (comment_text) => comment_text._highlightResult, {cascade: true})
    @JoinColumn({name: 'comment_text_id'})
    comment_text: Comment_textEntity;

    @OneToOne((type) => Story_titleEntity, (story_title) => story_title._highlightResult, {cascade: true})
    @JoinColumn({name: 'story_title_id'})
    story_title: Story_titleEntity;

    @OneToOne((type) => Story_urlEntity, (story_url) => story_url._highlightResult, {cascade: true})
    @JoinColumn({name: 'story_url_id'})
    story_url: Story_urlEntity;
}
