import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { HighlightResultsEntity } from "./highlightresults.entity";

@Entity({name: 'Author'})
export class AuthorEntity{

    @PrimaryGeneratedColumn('increment')
    id:number;

    @Column()
    value:string;

    @Column()
    matchLevel:string;

    @Column()
    matchedWords:string;

    @OneToOne((type) => HighlightResultsEntity, (highlightResult) => highlightResult.author)
    _highlightResult: HighlightResultsEntity;
}