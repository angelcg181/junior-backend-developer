import { Column, Entity, OneToOne, PrimaryGeneratedColumn } from "typeorm";
import { HighlightResultsEntity } from "./highlightresults.entity";

@Entity({name: 'Story_title'})

export class Story_titleEntity{

    @PrimaryGeneratedColumn('increment')
    id:number;

    @Column()
    value:string;

    @Column()
    matchLevel:string;
    
    @Column()
    matchedWords:string;

    @OneToOne((type) => HighlightResultsEntity, (highlightResult) => highlightResult.story_title)
    _highlightResult: HighlightResultsEntity;

}