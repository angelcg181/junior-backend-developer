import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { NewsModule } from './modules/news/news.module';

@Module({
  imports: [NewsModule, TypeOrmModule.forRoot({
    type: 'postgres',
    host: 'localhost',
    port: 5432,
    username: 'admin',
    password: 'my-password',
    database: 'postgres',
    autoLoadEntities: true,
    synchronize: true
  })]
})
export class AppModule {}
